# SSC0955 – Introdução a Sistemas Computacionais
SSC0955 – Introdução a Sistemas Computacionais

- Professor: Eduardo do Valle Simões
- Email: simoes@@@@@@@icmc.usp.br
- Departamento de Sistemas de Computação – ICMC - USP
- Grupo de Sistemas Embarcados e Evolutivos
- Laboratório de Computação Reconfigurável

## Alunos de 2022 - Primeiro semestre 

- Lista de Presença - Favor assinar a Lista de Presença durante o horario das aulas
- Essa disciplina irá utilizar o projeto do Processador ICMC, que é um processador RISC de 16 bits implementado em FPGA - todas as ferramentas de software e hardware estão disponíveis no Github do projeto: https://github.com/simoesusp/Processador-ICMC
- Os alunos serão divididos em grupos de 3-4 alunos para implementação do trabalho prático para a avaliação 
- TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE!!


## Apresentação dos Trabalhos: Insira as informações do seu grupo no arquivo do GoogleDocs:
- https://docs.google.com/spreadsheets/d/1g0QIn2H8DrhZ0gjGCgDpbqRuJYWvuqok-dUTlm9mNbk/edit?usp=sharing



## Processador ICMC - https://github.com/simoesusp/Processador-ICMC

- Nossa disciplina irá usar um Processador desenvolvido pelos próprios alunos do ICMC disponível neste repositório do github

## Simulador para programação em Assembly 
- Nossa disciplina irá usar um simulador para desenvolver programas em linguagem Assembly que poderá ser encontrado para Windows, Linux e MacOS em: https://github.com/simoesusp/Processador-ICMC/blob/master/Install_Packages/
- Para Windows, fiz um link super fácil de instalar: https://github.com/simoesusp/Processador-ICMC/blob/master/Install_Packages/Simulador_Windows_Tudo_Pronto_F%C3%A1cil%20(1).zip
  - Esse zip já vem inclusive com o sublime configurado para escrever o software (incluindo a sintaxe highlight) e o montador e o simulador já configurado para ser chamado com a tecla F7
  - Para instalar basta fazer o download na area de trabalho ou na pasta Documentos
  - Entrar na pasta ..\Simulador\Sublime Text 3
  - Executar o sublime: "sublime_text.exe"
  - Se ele pedir, NÃ0 FAÇA O UPDATE !!!!!!!!!!!!!!!
  - Vá em File - Open File e volte uma pasta para ..\Simulador\
  - Abra o sw em Assembly chamado Hello4.ASM
  - Teste se está tudo funcionando chamando o MONTADOR e o SIMULADOR com a tecla F7
  - Apartir daí pode-se salvar o sw com outro nome e fazer novos programas
  - Apenas preste atenção para estar na pasta ..\Simulador\
  - Se der o erro: [Decode error - output not utf-8] é porque você não está na pasta ..\Simulador\
  - ... Ou basta mudar o formato para utf-8 e salvar...


## Avaliação
- A avaliação será por meio de uma PROVA (50% da nota) sobre o conteúdo teórico e da apresentação de um TRABALHO (50% da nota) - Implementação de um Jogo em Linguagem Montadora (Assembly) no simulador (links acima)
- Os alunos serão divididos em grupos de 3-4 alunos para implementação do trabalho prático para a avaliação 
- TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE!!

### Apresentação dos trabalhos Turma 2022 - Primeiro semestre:
- Apresentação dos trabalhos será em reunião PRESENCIAL com o Professor nas últimas semanas de aula: marcar o dia e horário escolhidos para apresentacao na tabela do arquivo:  ==> LINK DOCS: https://docs.google.com/spreadsheets/d/17mreXcM4h2Zd4iVo86YvlOYo_1hwc4qkne-tgX1RUCY/edit?usp=sharing

  - Apresentação dos trabalhos será presencial na Sala de aula nas últimas semanas do Semestre

  - Insira seu projeto e reserve um horario para apresentar no arquivo do GoogleDocs, informando: TÍTULO DO PROJETO - nomes dos alunos - NUMERO USP dos alunos - link pro github/gitlab do seu Trabalho

## Trabalho da Disciplina:
- 1) Implementar um Jogo em Assembly (tema Livre)
- 2) Apresentar o Jogo rodando no simulador do Processador para o Professor

### Documentação do Trabalho no Github/Gitlab
- Criar uma conta sua no Github ou no Gitlab
- Os projetos devem conter um Readme explicando o projeto e o software deve estar muito bem comentado!!
- Incluir no seu Github/Gitlab: o JOGO.ASM e caso tenha alterado, o CHARMAP.MIF. 
- Obrigatório: incluir um VÍDEO DE VOCË explicando o projeto (pode ser somente uma captura de tela...) - Upa o vídeo no youtube ou no drive e poe o link no Readme. ==> Não coloque o Vídeo no Github/Gitlab
- Além do VÍDEO DE VOCË explicando o projeto, TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE na Sala de aula




# Material Didático - 2022 - Primeiro Semestre

## Processador ICMC
- Link para o projeto do nosso próprio Processador: https://github.com/simoesusp/Processador-ICMC

## Simulador em C
- Link para o código fonte do Simple Simulator: https://github.com/simoesusp/Processador-ICMC/tree/master/Simple_Simulator

## Desenho da Arquitetura do nosso Processador ICMC
- Link para visualisar o Desenho na Ferramenta FIGMA: https://www.figma.com/file/mHQmhfvLiwmIRXkHlFDTyn/CPU_ICMC-team-library?node-id=415%3A589

## Arquitetura do nosso Processador ICMC:  https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0180-Eletronica-para-Computacao/SSC0511-Organizacao-de-Computadores-Digitais_instrucoes_10_2016.pdf

## Gerador de Caracteres e Telas do Gustavo: https://github.com/GustavoSelhorstMarconi/Create-Screens-in-Assembly-with-python

# Aulas 2022

### Aula 1
- Apresentacao da Disciplina

### Aula 2
- Revisão e introdução geral ao conteudo de toda a Disciplina.
  - Transistores -> Portas Lógicas -> Registradores, Memória, ULA -> Maquina de controle -> Arquitetura de um Processador -> RISC vs CISC -> Pipeline -> Paralelismo -> MIMD (Multiple Instruction Stream, Multiple Dada Stream)

## Aula 3
### Roteiro do conteúdo a ser coberto nas proximas aulas:
- 1.-	Overclock
  - 1.1-	Aquecimento, refrigeração
  - 1.2-	Operação de uma CPU
  - 1.3-	Lei de Ohm
  - 1.4-	O transistor como chave - MOSFET
  - 1.5-	Construção de portas lógicas com transistores
  - 1.6-	Atrasos lógicos na propagação de sinais
  - 1.7-	Capacitores
  - 1.8-	Fan-out

- Material: 
  - Konrad Zuse - http://www.geocities.ws/hifi_eventos/Z1.html

  - Arquitetura do nosso Processador ICMC - https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0180-Eletronica-para-Computacao/SSC0511-Organizacao-de-Computadores-Digitais_instrucoes_10_2016.pdf

  - Maq Controle Babage Arduino - iTnkercad: https://www.tinkercad.com/things/4EVIesUUlub
  - Linguagem de Programação do Babage - https://docs.google.com/document/d/1o4VXz_-rqT4DdeEFDnQiyRqTgo9NAKdWRpq_G3zO0YQ/edit?usp=sharing


  - Porta NOT (nMOS e CMOS) http://tinyurl.com/uk6az46

  - Liga LED e tensão de ativação do transistor - https://tinyurl.com/yhbob6lz

  - Porta AND (nMOS) http://tinyurl.com/ukexy4p

  - Porta OR (nMOS) http://tinyurl.com/tkajd8r

  - Porta AND (CMOS) http://tinyurl.com/wd6xpb7

  - Porta OR (CMOS) http://tinyurl.com/rp6qbq5

  - Capacitor - https://tinyurl.com/y6tyo82m

  - How to cook hotdogs with a 20000 volt capacitor bank - https://www.youtube.com/watch?v=DQ67njnNaxw 

  - Cadeia NOT FANOUT(nMOS) http://tinyurl.com/sjp8cq8

  - Cadeia NOT FANOUT(CMOS) http://tinyurl.com/vke6tj7

  - Cadeia NOT FANOUT 5V-10V (CMOS) http://tinyurl.com/ua8m7xh

## Aula 4 --> Programação em linguagem Montadora (Assembly)

- Compilando um Programa de C -> Assembly -> Binary - https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0511-Organizacao-de-Computadores-Digitais/MaterialAulaDistancia/Compilando_um_Programa.c

- MONTADOR - https://github.com/simoesusp/Processador-ICMC/tree/master/NovoMontadorLinux

- SIMULADOR - https://github.com/simoesusp/Processador-ICMC/tree/master/Install_Packages

- Link para o projeto do nosso próprio Processador: https://github.com/simoesusp/Processador-ICMC

- RPG bem legal -> Battle of Tiers: Jonas Wendel Costa de Souza - https://github.com/4Vertrigo/BattleOfTiers


## Aula 5 --> Implementacao do nosso Processador
### Roteiro do conteúdo a ser coberto nas proximas aulas:
- 1.-	Apresentacao das Ferramentas:
  - 1.1-	Simulador Normal
  - 1.2-  Simple Simulator
  - 1.3-  Testa CPU
  - 1.4-  Desenho na ferramenta FIGMA
- 2. Projeto das Instrucoes do Processador
  - 2.1-  NOP
  - 2.2-  LOADN
  - 2.3-  LOAD
  - 2.4-  STORE
  ...

- Material: 
  - Link para a Aula Gravada (08/10/21) - https://drive.google.com/file/d/1b259fT0bFdzSXMUy-jr_eYg1NE_kgxpX/view?usp=sharing
  - Link para visualisar o Desenho na Ferramenta FIGMA: https://www.figma.com/file/mHQmhfvLiwmIRXkHlFDTyn/CPU_ICMC-team-library?node-id=415%3A589

  - Implementacao do nosso Processador: Loadi, Storei e Move
- Link para a Aula Gravada (19/10/21) - https://drive.google.com/file/d/1ORU9T3jdLUY6PiTUyIHH7a-jEqGmioN2/view?usp=sharing

## Aula 6 --> Memória e Cache

## Aula 7 --> Periféricos e Barramentos
- Transparencias sobre Barramentos: https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0511-Organizacao-de-Computadores-Digitais/MaterialAulaDistancia/A02_Barramento_short2.odp
- Video explicando Barramentos (assistir a partir de 1h:15m:00s ) - https://drive.google.com/file/d/1IJDyGPvWEbK4dU3slQh02yU3WE4WNi7D/view?usp=sharing

## Aula 8 --> RISC vs CISC

## Aula 9 --> Pipeline 

## Aula 10 --> Paralelismo -> MIMD (Multiple Instruction Stream, Multiple Dada Stream)



# Apresentação da Disciplina

Professor: Eduardo do Valle Simões
Email: Simoes AT icmc.usp.br
Departamento de Sistemas de Computação – ICMC - USP
Grupo de Sistemas Embarcados e Evolutivos
Laboratório de Computação Reconfigurável

Disciplina: SSC0955 - Introdução a Sistemas Computacionais
Introduction to Computer Systems

Créditos Aula:	4
Créditos Trabalho:	1
Carga Horária Total:	90 h

### Objetivos
Introduzir conceitos de Lógica Digital, Organização e Arquitetura de Computadores.
 
### Programa Resumido
Lógica booleana. Circuitos. Arquitetura de von Neumman. Arquiteturas avançadas e paralelas.
 
### Programa
Álgebra de Boole. Noções de circuitos combinacionais e sequenciais. Arquitetura de von Neumman: processador (elementos básicos, operação geral), sistema de memória, sistema de E/S, barramentos. Arquiteturas avançadas: pipeline, superescalar, multithreading. Arquiteturas paralelas: classificação de Flynn, arquiteturas MIMD.
 
 
 
### Avaliação
     	
Método
Exposição dos conteúdos. Desenvolvimento de exercícios e trabalhos práticos durante as aulas e extra-classe.
Critério
Serão atribuídas notas a exercícios e trabalhos práticos desenvolvidos durante o curso, bem como às avaliações (provas) aplicadas durante o semestre. A nota final representará a média das notas obtidas pelo aluno no decorrer do semestre.
Norma de Recuperação
Para a aprovação pela recuperação (nota final < 5), deve-se usar este critério: (NP-2) / 5 * Mrec + 7 - NP, se Mrec >= 5; ou Max { NP, Mrec }, se Mrec < 5.
 
### Bibliografia
     	
- IDOETA,I.V.; CAPUANO, F.G. Elementos de Eletrônica Digital, 35 ed., São Paulo, Livros Érica, 2003 
- TANENBAUM, A.S. Organização e Estrutura de Computadores, Prentice Hall, 5th ed, 2007. 
- STALLINGS, W. Arquitetura e Organização de Computadores, Prentice Hall, 5a. ed., 2002.


### Metodologia:

- As aulas serão principalmente expositivas explicando-se na lousa os conteúdos do livro. Sendo assim, a cópia do material dado na lousa possibilitará a obtenção de uma síntese bem exemplificada do conteúdo do livro.
- Serão utilizadas transparências, apresentando os tópicos: arquitetura de processadores, elementos básicos, vários exemplos de hierarquia e controle (arbitragem) de barramentos, E/S Programada, configurações de memória e o conceito, operação e exemplos de DMA.
- Será disponibilizada uma ampla lista de exercícios sobre toda a matéria e marcada uma aula específica para tirar as duvidas dos alunos sobre os exercícios.
- Serão apresentadas em aula e disponibilizadas aos alunos ferramentas de CAD para a programação e simulação de processadores. Nestas ferramentas, serão apresentados a arquitetura do processador, seu mapa de memória, conjunto de instruções e programas em Assembly controlando dispositivos como sensores, motores e esteiras e alguns jogos.
- Será disponibilizada uma ferramenta que é um simulador de operação de um processador bastante semelhante ao que será apresentado e implementado em aula, desenvolvida por alunos veteranos do ICMC, incluindo os códigos (feitos em linguagem C) do algoritmo que simula os módulos do processador, o algoritmo da máquina de controle e um montador para os programas em linguagem de máquina. Com esta ferramenta, os alunos podem editar o processador, incluir novas instruções, reprogramar a máquina de controle para controlar estas novas instruções e editar o montador para montá-las para o código de maquina do processador.
- Serão também oferecidas aulas opcionais para a turma no laboratório, na qual serão apresentados a linguagem VHDL e a utilização das placas de FPGA recentemente adquiridas para a implementação de processadores próprios dos alunos. Serão fornecidas transparências com a síntese desta metodologia para os alunos.
- Alguns alunos serão convidados a apresentar implementações para seus colegas utilizando as ferramentas apresentadas para implementar software em linguagem montadora e controlar dispositivos com os seus microcontroladores, bem como os que implementarem processadores próprios em software com o simulador ou em hardware na placa de FPGA.
- Todo este material será disponibilizado diretamente aos alunos durante as aulas e através do github.


